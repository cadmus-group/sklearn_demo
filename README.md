# Conda and Scikit Learn

This is intended as a follow up to the first Python session: 
https://gitlab.com/cadmus-group/python_demo

The goals of this session is to introduce scikit-learn and conda for local analysis.
* https://scikit-learn.org/stable/tutorial/statistical_inference/index.html
* https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html

## Prerequisites
You will need to have the following installed on your Windows machine
* Anaconda

## Setup

You will need to have this repository on your local machine. You can either
use Git to `git clone` the files or you can download a zip.

### down load zip instructions

Head to the Gitlab site: https://gitlab.com/cadmus-group/sklearn_demo

On the right side of the screen a little ways down there is a button
with at cloud and a down arrow. This is the download button. Click it
and select "Download Zip"

![img](images/zip_download.PNG)

When prompted, choose "open with Windows Explorer"

![img](images/open_with.PNG)

Choose where you'd like this contents of this folder to go. Your `OneDrive - Cadmus` folder 
is an excellent choice. Then drag the contents from the extracted zip into this location.

![img](images/drag_folder.PNG)


### run Jupyter


We will be using the base conda virtual environment to run jupyter. Hit the Windows key
and then start typing Jupyter and you should see the Jupyter application pop up.


![img](images/jupyter_launch.PNG)

Running this program will launch a new page in your default web browser which gives you the 
interface for browsing your file system. Navigate to where you placed the
unzipped folder and open the notebooks folder. 

![img](images/to_notebooks.PNG)

Now if you select any of these files with the `.ipynb` extension
 you will "launch the notebook" in a new tab.

![img](images/nb.PNG)





This is a great introduction to the Jupyter Notebook interface:
https://realpython.com/jupyter-notebook-introduction/ 